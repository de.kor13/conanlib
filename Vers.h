#ifndef VERS_H
#define VERS_H

#include <openssl/ssl.h>
#include <iostream>

class Vers {
public:
    Vers();
    ~Vers();

    void printOpenSSLVersion();
};

#endif // VERS_H


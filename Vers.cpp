#include "Vers.h"

Vers::Vers() {
    SSL_library_init();
}

Vers::~Vers() {
    // Дополнительные действия по очистке, если необходимо
}

void Vers::printOpenSSLVersion() {
    std::cout << "OpenSSL version: " << OpenSSL_version(OPENSSL_VERSION) << std::endl;
}
